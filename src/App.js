import React from "react";
import {Route} from 'react-router-dom'
import './App.css';
import Header from "./components/Header";
import SearchWidget from "./components/SearchWidget";
import DestinationList from "./components/DestinationList";
import Footer from "./components/Footer";
import {createMuiTheme, ThemeProvider} from "@material-ui/core";
import DestinationContent from "./components/DestinationContent";

const theme = createMuiTheme({
    palette: {
        primary: {
            // Purple and green play nicely together.
            main: '#1976d2',
            light: '#63a4ff',
            dark: '#004ba0',
            contrastText: '#ffffff'
        },
        secondary: {
            // This is green.A700 as hex.
            main: '#64b5f6',
            light: '#9be7ff',
            dark: '#2286c3',
            contrastText: '#ffffff'
        },
    },
});


const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <div className="App">
                <Route exact path={"/"} render={() => (
                    <div>
                        <Header />
                        <SearchWidget />
                        <DestinationList />
                        <Footer />
                    </div>
                )} />

                <Route exact path={"/destino/:id"} render={() => (
                    <div>
                        <Header />
                        <DestinationContent />
                        <Footer />
                    </div>
                )} />
            </div>
        </ThemeProvider>
    );
}

export default App;
