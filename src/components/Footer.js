import React from 'react';
import "../styles/footer.scss"

const Footer = () => {
    return (
        <footer className={"footer_wrapper"}>
            Projeto desenvolvido no âmbito da unidade curricular Programação Web do Mestrado em Tecnologias e Sistemas Informáticos Web
        </footer>
    );
};

export default Footer;