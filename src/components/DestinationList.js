import React from 'react';
import {Card, CardActionArea, CardContent, CardMedia, Grid, Typography} from "@material-ui/core";
import "../styles/content-block.scss";
import {useApp} from "../overmind";
import DestinationCard from "./DestinationCard";
import CircularProg from "./CircularProg";

const DestinationList = () => {
    const { state } = useApp();
    const {destinationList} = state;
    let isLoaded = (destinationList.length > 0);
    return (
        <div className={"content-block-wrapper"}>
            {isLoaded ? (
                <Grid container spacing={4}>
                    {destinationList.map((data, index ) => {
                        return(
                            <Grid item sm={6} md={4} lg={3} key={index}>
                                <DestinationCard destination={data} />
                            </Grid>
                        );
                    })}
                </Grid>
            ):(<CircularProg />)}
        </div>
    );
};

export default DestinationList;