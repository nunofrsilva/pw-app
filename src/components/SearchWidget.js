import React from "react";
import '../styles/search-widget.scss'
import SearchIcon from '@material-ui/icons/Search';
import {InputBase} from "@material-ui/core";
import {useApp} from "../overmind";

const SearchWidget = () => {
    const { state, actions } = useApp();
    const {inputSearch} = state;
    const handleSearch = (event) => {
        actions.search(event.target.value);
    }
    return (
        <div className={"search-widget-wrapper"}>
            <div>
                <h1>Procure o seu próximo destino de férias.</h1>
                <div className={"search-input"}>
                    <div className={'search_icon'}>
                        <SearchIcon />
                    </div>
                    <InputBase
                        placeholder="Pesquisar…"
                        classes={{
                            root: 'inputRoot',
                            input: 'inputInput',
                        }}
                        inputProps={{ 'aria-label': 'search' }}
                        onChange={handleSearch}
                        value={inputSearch}
                    />
                </div>
            </div>
        </div>
    );
};

export default SearchWidget;