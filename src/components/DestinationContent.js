import React, {useEffect} from 'react';
import {Container} from "@material-ui/core";
import "../styles/destination-content.scss"
import {useApp} from "../overmind";
import {useParams} from "react-router-dom";
import CircularProg from "./CircularProg";

const DestinationContent = () => {
    const {state, actions} = useApp();
    const {selectedDestination} = state;
    const { id } = useParams();
    let isLoaded = false;

    if (selectedDestination !== null && selectedDestination.id == id) {
        isLoaded = true;
    }
    useEffect(() => {
        actions.getDestination(id);
    }, [id]);

    return (
        <div className={"destination-content-wrapper"}>
            {isLoaded ? (
                <div>
                    <div className={"image-content"}>
                        <img src={selectedDestination.image} />
                    </div>
                    <Container className={"content-container"}>
                        <h1>{selectedDestination.title}</h1>
                        <p>
                            {selectedDestination.content}
                        </p>
                    </Container>
                </div>
                ):(
                <CircularProg />
                )}
        </div>
    );
};

export default DestinationContent;