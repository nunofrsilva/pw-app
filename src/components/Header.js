import React, {useState} from "react";
import {AppBar, Grid, IconButton, Menu, MenuItem, Toolbar, Typography} from "@material-ui/core";
import {AccountCircle} from "@material-ui/icons";
import '../styles/header.scss'
import {useApp} from "../overmind";
import {Link} from "react-router-dom";

const userNames = [
    {key: "anonymous", value:"Anónimo"},
    {key: "teenagers", value:"Jovem"},
    {key: "adult", value:"Adulto"},
    {key: "senior", value:"Sénior"}
];

const Header = () => {
    const [anchorEl, setAnchorEl] = useState(null);
    const {state, actions} = useApp();
    const open = Boolean(anchorEl);
    const {userType} = state;
    const username = userNames.find(d => d.key === userType);
    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleSelectUser = (userType) => (event) => {
        actions.changeUserType(userType);
        setAnchorEl(null);
    };

    return (
        <AppBar position="static">
            <Toolbar variant="regular" className={"toolbar_wrapper"}>
                <Link to={"/"}>
                    <Typography variant="h5">
                        Destinos
                    </Typography>
                </Link>

                <div>
                    <IconButton
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        onClick={handleMenu}
                        color="inherit"
                    >
                        <AccountCircle /> <span className={"user-name-header"}>{username.value}</span>
                    </IconButton>
                    <Menu
                        id="menu-appbar"
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        keepMounted
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={open}
                        //onClose={handleClose}
                    >
                        <MenuItem onClick={handleSelectUser("anonymous")} >Utilizador Anónimo</MenuItem>
                        <MenuItem onClick={handleSelectUser("teenagers")} >Utilizador Jovem</MenuItem>
                        <MenuItem onClick={handleSelectUser("adult")} >Utilizador Adulto</MenuItem>
                        <MenuItem onClick={handleSelectUser("senior")} >Utilizador Sénior</MenuItem>
                    </Menu>
                </div>
            </Toolbar>
        </AppBar>
    );
};

export default Header;