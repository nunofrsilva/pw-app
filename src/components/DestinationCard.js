import React from 'react';
import {Card, CardActionArea, CardContent, CardMedia, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";

const DestinationCard = ({destination}) => {
    return (
        <Link to={"/destino/"+destination.id} className={"item-link"}>
            <Card className={"card-wrapper"}>
                <CardActionArea>
                    <CardMedia
                        component="img"
                        alt="Contemplative Reptile"
                        height="140"
                        image={destination.image}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {destination.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {destination.content.split(' ').slice(0, 20).join(' ')}...
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Link>
    );
};

export default DestinationCard;
