export const setDestinationList = ({state}, destinationList) => {
    state.destinationList = [...destinationList];
};

export const search = async ({state, effects}, keyword) => {
    const {userType} = state;
    state.inputSearch = keyword;
    try {
        const {data: {data: destinationList = []}} = await effects.api.searchDestination(keyword, userType);
        const list = [];
        destinationList.forEach( d => {
            list.push(d.destination);
        });
        state.destinationList = [...list];
    } catch ({message}) {
        console.error("Error retrieving destination list", message);
    }
};

export const changeUserType = async ({state, effects}, userType) => {
    state.userType = userType;
    state.inputSearch = "";
    try {
        const {data: {data: destinationList = []}} = await effects.api.getDestinationList(userType);
        state.destinationList = [...destinationList];
    } catch ({message}) {
        console.error("Error retrieving destination list", message);
    }
};

export const getDestination = async ({state, effects}, id) => {
    try {
        const {data: {data: destination = {}}} = await effects.api.getDestination(id);
        state.selectedDestination = {...destination};
    } catch ({message}) {
        console.error("Error retrieving destination list", message);
    }
};
