export default ({actions, effects}) => {
    const {setDestinationList} = actions;
    (async () => {
        try {
            const {data: {data: destinationList = []}} = await effects.api.getDestinationList();
            setDestinationList(destinationList);
        } catch ({message}) {
            console.error("Error retrieving destination list", message);
        }
    })();
};